/* set css visibility untuk sebuah elemen */
function setvisibility(namaid,statusvis) {
  if (statusvis == "1") {
    document.getElementById(namaid).style.visibility = "visible";
  } else {
    document.getElementById(namaid).style.visibility = "hidden";
  }
}

/* set css display untuk sebuah elemen */
function setdisplay(namaid,statusdis) {
  if (statusdis == "1") {
    document.getElementById(namaid).style.display = "block";
  } else {
    document.getElementById(namaid).style.display = "none";
  }
}

/* fungsi-fungsi untuk menu di bagian atas */
var ddaktif = "";
function tampildd(id) {
    for (i = 0; i < jumlahdd; i++) {
        iddd = "dd" + i;
        setdisplay(iddd,0);
    }
    setdisplay(id,1);
}
function dalamdd(id) {
    ddaktif = id;
}
function luardd(id) {
    ddaktif = "";
    setTimeout("tutupdd()",300);
}
function tutupdd() {
    for (i = 0; i < jumlahdd; i++) {
        iddd = "dd" + i;
        if (ddaktif != iddd) setdisplay(iddd,0);
    }
}
